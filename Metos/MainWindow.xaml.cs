﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Metos.Infrastructure.Models;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace Metos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public static ObservableCollection<PacketInfomation> PacketInfomations =
            new ObservableCollection<PacketInfomation>();

        public static ObservableCollection<PacketInfomation> GridPacketInfomations =
            new ObservableCollection<PacketInfomation>();

        public static PacketInfomation PacketInfomation = new PacketInfomation();

        private readonly Controls.CaptureControl _captureControl = new Controls.CaptureControl(PacketInfomations, PacketInfomation);
        private readonly Controls.CompareResponseControl _compareResponseControl = new Controls.CompareResponseControl(PacketInfomations, PacketInfomation);
        private readonly Controls.LoadTestControl _loadTestControl = new Controls.LoadTestControl(PacketInfomations, PacketInfomation);
        private readonly Controls.PlaybackControl _playbackControl = new Controls.PlaybackControl(PacketInfomations, PacketInfomation);
        private readonly Controls.CustomControl _customControl = new Controls.CustomControl(PacketInfomations, PacketInfomation);
        private readonly Timer _timer = new Timer(100);

        public MainWindow()
        {
            InitializeComponent();
            WorkspaceContentControl.Content = _captureControl;
            PacketInfomations.CollectionChanged += PacketInfomations_CollectionChanged;
            DataGrid.ItemsSource = GridPacketInfomations;
            _timer.Elapsed += _timer_Elapsed;
            DataGrid.LoadingRow += (s, e) =>
            {
                e.Row.ToolTip = ((PacketInfomation)e.Row.Item).Url;
            };
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(UpdateDataGrid);
            _timer.Stop();
        }

        private void PacketInfomations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            StartUpdateDataGrid();
        }

        private void StartUpdateDataGrid()
        {
            _timer.Stop();
            _timer.Start();
        }

        private void UpdateDataGrid()
        {
            GridPacketInfomations.Clear();
            foreach (PacketInfomation dataGridSelectedItem in PacketInfomations)
            {
                dataGridSelectedItem.IsSeleted = false;
            }
            foreach (var packetInfomation in PacketInfomations.Where(q => (string.IsNullOrEmpty(SearchTextBox.Text) || q.Url.ToLower().Contains(SearchTextBox.Text.ToLower())) &&
                                                                          (RequestMethordComboBox.Text == "ALL" || q.RequestMethod == RequestMethordComboBox.Text)))
            {
                if (!GridPacketInfomations.Contains(packetInfomation))
                {
                    GridPacketInfomations.Add(packetInfomation);
                }
            }
        }

        private void ResetAllMenuButton(ToggleButton selectedButton)
        {
            CaptureButton.IsChecked = false;
            CaptureButton.IsEnabled = true;
            CompareResponseButton.IsChecked = false;
            CompareResponseButton.IsEnabled = true;
            LoadTestResponseButton.IsChecked = false;
            LoadTestResponseButton.IsEnabled = true;
            PlaybackResponseButton.IsChecked = false;
            PlaybackResponseButton.IsEnabled = true;
            PlaybackResponseButton.IsChecked = false;
            PlaybackResponseButton.IsEnabled = true;
            CustomButton.IsChecked = false;
            CustomButton.IsEnabled = true;
            selectedButton.IsChecked = true;
            selectedButton.IsEnabled = false;
        }

        private void LoadTestResponseButton_OnClick(object sender, RoutedEventArgs e)
        {
            ResetAllMenuButton(LoadTestResponseButton);
            WorkspaceContentControl.Content = _loadTestControl;
        }

        private void CaptureButton_OnClick(object sender, RoutedEventArgs e)
        {
            ResetAllMenuButton(CaptureButton);
            WorkspaceContentControl.Content = _captureControl;
        }

        private void PlaybackResponseButton_OnClick(object sender, RoutedEventArgs e)
        {
            ResetAllMenuButton(PlaybackResponseButton);
            WorkspaceContentControl.Content = _playbackControl;
        }

        private void CompareResponseButton_OnClick(object sender, RoutedEventArgs e)
        {
            ResetAllMenuButton(CompareResponseButton);
            WorkspaceContentControl.Content = _compareResponseControl;
        }

        private void CustomButton_OnClick(object sender, RoutedEventArgs e)
        {
            ResetAllMenuButton(CustomButton);
            WorkspaceContentControl.Content = _customControl;
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog { Filter = "Json File (.json)|*.json" };
            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                var data = JsonConvert.SerializeObject(PacketInfomations.ToList(), Formatting.Indented);
                File.WriteAllText(dialog.FileName, data);
            }
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DataGrid.SelectedItems.Count > 0)
            {
                List<PacketInfomation> selectedItemsList = DataGrid.SelectedItems.Cast<PacketInfomation>().ToList();
                foreach (var item in selectedItemsList)
                {
                    PacketInfomations.Remove(item);
                }
            }
        }

        private void ClearButton_OnClick(object sender, RoutedEventArgs e)
        {
            ClearData();
        }

        private async void ClearData()
        {
            var messageDialogResult = await this.ShowMessageAsync("Clear all captures", "This will clear all capture, continue?", MessageDialogStyle.AffirmativeAndNegative);
            if (messageDialogResult == MessageDialogResult.Affirmative)
            {
                PacketInfomations.Clear();
            }
        }

        private void LoadButton_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog { Filter = "Json File (.json)|*.json" };
            dialog.Multiselect = true;
            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                foreach (var fileName in dialog.FileNames)
                {
                    var text = File.ReadAllText(fileName);
                    var data = JsonConvert.DeserializeObject<List<PacketInfomation>>(text);
                    foreach (var packetInfomation in data)
                    {
                        PacketInfomations.Add(packetInfomation);
                    }
                }
            }
        }

        private void DataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataGrid.SelectedItem != null)
            {
                DetailDataTextbox.Text = ((PacketInfomation)DataGrid.SelectedItem).Details;
                ResponseTextbox.Text = ((PacketInfomation)DataGrid.SelectedItem).Response;
                PacketInfomation.Load((PacketInfomation)DataGrid.SelectedItem);
            }
            if (DataGrid.SelectedItems.Count > 0)
            {
                foreach (PacketInfomation dataGridSelectedItem in DataGrid.Items)
                {
                    dataGridSelectedItem.IsSeleted = DataGrid.SelectedItems.Contains(dataGridSelectedItem);
                }
            }
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            _captureControl.Stop();
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var addurlWindow = new AddUrlWindow { Owner = this };
            addurlWindow.ShowDialog();
            if (addurlWindow.PacketInfomation != null)
            {
                PacketInfomations.Add(addurlWindow.PacketInfomation);
            }
        }

        private void DataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DataGrid.SelectedItem != null)
            {
                var addurlWindow = new AddUrlWindow((PacketInfomation)DataGrid.SelectedItem) { Owner = this };
                addurlWindow.ShowDialog();
                //PacketInfomations.Add(addurlWindow.PacketInfomation);
            }
        }

        private void SearchTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            StartUpdateDataGrid();
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StartUpdateDataGrid();
        }

        private void AboutButton_OnClick(object sender, RoutedEventArgs e)
        {
            var licence = @"Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License";
            var s = this.ShowMessageAsync("About", "Devoloped by Taufiq Abdur Rahman" + Environment.NewLine + "License Under" + Environment.NewLine + licence);
        }
    }
}