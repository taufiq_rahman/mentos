﻿using Metos.Infrastructure.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Metos.Controls
{
    /// <summary>
    ///     Interaction logic for CompareResponseControl.xaml
    /// </summary>
    public partial class PlaybackControl : IBaseControl
    {
        private readonly ObservableCollection<PacketInfomation> _packetInfomations;

        public readonly ObservableCollection<PacketInfomationResult> PacketInfomationResultList =
            new ObservableCollection<PacketInfomationResult>();

        public readonly ObservableCollection<UrlReplaceData> UrlReplaceDataList =
            new ObservableCollection<UrlReplaceData>();

        private Thread _thread;

        public PlaybackControl(ObservableCollection<PacketInfomation> packetInfomations,
            PacketInfomation packetInfomation)
        {
            _packetInfomations = packetInfomations;
            InitializeComponent();
            ResultDataGrid.ItemsSource = PacketInfomationResultList;
            UrlDataGrid.ItemsSource = UrlReplaceDataList;
        }

        private void ExcuteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsList = PacketInfomationResultList.ToList();
            OnExcution(true);
            selectedItemsList.ForEach(q =>
            {
                q.NewResponse = "";
                q.Result = "--";
                q.Status = "Pending";
            });
            if (_thread == null || !_thread.IsAlive)
                _thread = new Thread(() =>
                {
                    foreach (var packetInfomation in selectedItemsList)
                        Excute(packetInfomation, UrlReplaceDataList.ToList());
                    Application.Current.Dispatcher.Invoke(() => { OnExcution(false); });
                });
            _thread.Start();
        }

        private void OnExcution(bool isexcuting)
        {
            ExcuteButton.IsEnabled = !isexcuting;
            AddButton.IsEnabled = !isexcuting;
            RemoveButton.IsEnabled = !isexcuting;
        }

        private void Excute(PacketInfomationResult packetInfomation, List<UrlReplaceData> urlReplaceDataList)
        {
            packetInfomation.Status = "Started";
            var url = packetInfomation.Url;
            var newUrl = "";
            foreach (var urlReplaceData in urlReplaceDataList)
                newUrl = url.Replace(urlReplaceData.Url, urlReplaceData.ReplaceWithUrl);

            packetInfomation.NewUrl = newUrl;
            HttpResponseMessage call1Result;
            try
            {
                using (var client2 = new HttpClient())
                {
                    HttpResponseMessage call2Result;
                    foreach (var packetInfomationHeader in packetInfomation.Headers)
                        client2.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key,
                            packetInfomationHeader.Value);
                    if (packetInfomation.RequestMethod == "GET")
                    {
                        call2Result = client2.GetAsync(new Uri(url)).Result;
                    }
                    else
                    {
                        var content = new StringContent(packetInfomation.Body);
                        call2Result = client2.PostAsync(new Uri(url), content).Result;
                    }

                    var result2 = call2Result.Content.ReadAsStringAsync().Result;
                    packetInfomation.Response = result2;
                }

                packetInfomation.Status = "Completed";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                packetInfomation.Status = "Failed";
            }
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var packetInfomations = _packetInfomations.ToList();

            foreach (var packetInfomation in packetInfomations.Where(q => q.IsSeleted))
            {
                var packetInfomationResult = new PacketInfomationResult(packetInfomation);
                packetInfomationResult.NewUrl = packetInfomationResult.Url;
                PacketInfomationResultList.Add(packetInfomationResult);
            }
        }

        private void RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsList = ResultDataGrid.SelectedItems.Cast<PacketInfomationResult>().ToList();
            foreach (var packetInfomation in selectedItemsList) PacketInfomationResultList.Remove(packetInfomation);
        }

        private void ResultDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var result = new PacketInfomation();
            result.Load((PacketInfomation)ResultDataGrid.SelectedItem);
            if (result != null)
            {
                SelectedUrlInfomationTextBox.Text = JsonConvert.SerializeObject(result, Formatting.Indented);
                UrlTextBox.Text = result.Response;
            }
        }

        private void AddUrlButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ReplaceUrlTextBox.Text) && !string.IsNullOrEmpty(ReplaceToUrlTextBox.Text))
                UrlReplaceDataList.Add(new UrlReplaceData
                {
                    Url = ReplaceUrlTextBox.Text,
                    ReplaceWithUrl = ReplaceToUrlTextBox.Text
                });
        }

        private void RemoveUrlButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsList = UrlDataGrid.SelectedItems.Cast<UrlReplaceData>().ToList();
            foreach (var urlReplaceData in selectedItemsList) UrlReplaceDataList.Remove(urlReplaceData);
        }

        private void UrlDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            _thread?.Abort();
            OnExcution(false);
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = (PacketInfomationResult)ResultDataGrid.SelectedItem;
                result?.Load(JsonConvert.DeserializeObject<PacketInfomationResult>(SelectedUrlInfomationTextBox.Text));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            //UrlTextBox.Text = result.Response;
        }
    }
}