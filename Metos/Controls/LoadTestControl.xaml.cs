﻿using Metos.Infrastructure.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Metos.Controls
{
    /// <summary>
    /// Interaction logic for CompareResponseControl.xaml
    /// </summary>
    public partial class LoadTestControl : IBaseControl
    {
        private readonly ObservableCollection<PacketInfomation> _packetInfomations;
        private readonly PacketInfomation _packetInfomation;
        public readonly ObservableCollection<PacketInfomationResult> PacketInfomationResultList = new ObservableCollection<PacketInfomationResult>();
        public readonly ObservableCollection<UrlReplaceData> UrlReplaceDataList = new ObservableCollection<UrlReplaceData>();
        private Thread _thread;

        public LoadTestControl(ObservableCollection<PacketInfomation> packetInfomations, PacketInfomation packetInfomation)
        {
            _packetInfomations = packetInfomations;
            _packetInfomation = packetInfomation;
            InitializeComponent();
            ResultDataGrid.ItemsSource = PacketInfomationResultList;
            UrlDataGrid.ItemsSource = UrlReplaceDataList;
            _packetInfomation.SelectionChanged += _packetInfomation_SelectionChanged;
        }

        private void _packetInfomation_SelectionChanged(object sender, EventArgs e)
        {
            SelectedUrlInfomationTextBox.Text = JsonConvert.SerializeObject(_packetInfomation, Formatting.Indented);
        }

        private void OnExcution(bool isexcuting)
        {
            ExcuteButton.IsEnabled = !isexcuting;
        }

        private void ExcuteButton_OnClick(object sender, RoutedEventArgs e)
        {
            PrepareList();
            OnExcution(true);
            List<PacketInfomationResult> selectedItemsList = PacketInfomationResultList.ToList();
            var timeout = PauseNumaricUpDown.Value;
            var numberofThread = ThreadNumaricUpDown.Value;
            selectedItemsList.ForEach(q =>
            {
                q.NewResponse = "";
                q.Result = "--";
                q.Status = "Pending";
            });
            if (_thread == null || !_thread.IsAlive)
            {
                _thread = new Thread(() =>
                {
                    Parallel.For(0, selectedItemsList.Count,
                        new ParallelOptions { MaxDegreeOfParallelism = Convert.ToInt32(numberofThread) }, (s) =>
                          {
                              Excute(selectedItemsList[s], UrlReplaceDataList.ToList());
                              Thread.Sleep(Convert.ToInt32(timeout));
                          });
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        OnExcution(false);
                    });
                });
                _thread.Start();
            }
        }

        private void PrepareList()
        {
            PacketInfomationResultList.Clear();
            for (int i = 0; i < GetCount(UrlReplaceDataList.ToList()); i++)
            {
                var data = SelectedUrlInfomationTextBox.Text;
                foreach (var urlReplaceData in UrlReplaceDataList)
                {
                    var replacewith = Replace(urlReplaceData.ReplaceWithUrl, i);
                    data = data.Replace(urlReplaceData.Url, replacewith);
                }
                var packet = JsonConvert.DeserializeObject<PacketInfomationResult>(data);
                PacketInfomationResultList.Add(new PacketInfomationResult(packet));
            }
        }

        private int GetCount(List<UrlReplaceData> urlReplaceDataList)
        {
            var result = 1;
            foreach (var urlReplaceData in urlReplaceDataList)
            {
                var regexPartter = new Regex(@"\[\d+-\d+\]");
                var matches = regexPartter.Matches(urlReplaceData.ReplaceWithUrl);
                var regex = new Regex(@"\d+");
                foreach (Match m in matches)
                {
                    matches = regex.Matches(m.Value);
                    foreach (Match match in matches)
                    {
                        result += int.Parse(match.Value);
                    }
                }
            }

            return result;
        }

        private string Replace(string replaceWithUrl, int i)
        {
            var regexPartter = new Regex(@"\[\d+-\d+\]");
            var matches = regexPartter.Matches(replaceWithUrl);
            var regex = new Regex(@"\d+");
            if (matches.Count > 0)
            {
                Match m = matches[0];
                matches = regex.Matches(m.Value);
                if (matches.Count > 0)
                {
                    var startwith = int.Parse(matches[0].Value);
                    replaceWithUrl = regexPartter.Replace(replaceWithUrl, (startwith + i).ToString());
                }
            }

            return replaceWithUrl;
        }

        private void Excute(PacketInfomationResult packetInfomation, List<UrlReplaceData> urlReplaceDataList)
        {
            packetInfomation.Status = "Started";
            var url = packetInfomation.Url;
            var newUrl = "";
            foreach (var urlReplaceData in urlReplaceDataList)
            {
                newUrl = url.Replace(urlReplaceData.Url, urlReplaceData.ReplaceWithUrl);
            }

            packetInfomation.NewUrl = newUrl;
            HttpResponseMessage call1Result;
            try
            {
                var watch = Stopwatch.StartNew();
                using (var client1 = new HttpClient())
                {
                    foreach (var packetInfomationHeader in packetInfomation.Headers)
                    {
                        client1.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key, packetInfomationHeader.Value);
                    }
                    if (packetInfomation.RequestMethod == "GET")
                    {
                        call1Result = client1.GetAsync(new Uri(url)).Result;
                    }
                    else
                    {
                        var content = new StringContent(packetInfomation.Body, Encoding.UTF8, "application/json");
                        call1Result = client1.PostAsync(new Uri(url), content).Result;
                    }
                }
                packetInfomation.ResponceTime = watch.Elapsed;

                packetInfomation.Status = "Completed";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                packetInfomation.Status = "Failed";
                packetInfomation.Result = "--";
            }
        }

        private void ResultDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var result = (PacketInfomationResult)ResultDataGrid.SelectedItem;
            if (result != null)
            {
                UrlTextBox.Text = result.Response;
                //ResultTextBox.Text = result.NewResponse;
            }
        }

        private void AddUrlButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ReplaceUrlTextBox.Text) && !string.IsNullOrEmpty(ReplaceToUrlTextBox.Text))
            {
                UrlReplaceDataList.Add(new UrlReplaceData
                {
                    Url = ReplaceUrlTextBox.Text,
                    ReplaceWithUrl = ReplaceToUrlTextBox.Text
                });
            }
        }

        private void RemoveUrlButton_OnClick(object sender, RoutedEventArgs e)
        {
            List<UrlReplaceData> selectedItemsList = UrlDataGrid.SelectedItems.Cast<UrlReplaceData>().ToList();
            foreach (UrlReplaceData urlReplaceData in selectedItemsList)
            {
                UrlReplaceDataList.Remove(urlReplaceData);
            }
        }

        private void UrlDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            _thread.Abort();
            OnExcution(false);
        }
    }
}