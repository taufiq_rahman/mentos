﻿using CefSharp;
using Fiddler;
using Metos.Infrastructure.Models;
using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Metos.Controls
{
    public partial class CaptureControl : UserControl, IBaseControl
    {
        private readonly ObservableCollection<PacketInfomation> _packetInfomations;

        public CaptureControl(ObservableCollection<PacketInfomation> packetInfomations, PacketInfomation packetInfomation)
        {
            _packetInfomations = packetInfomations;
            var path = System.AppDomain.CurrentDomain.BaseDirectory;
            CefSettings cfsettings = new CefSettings();
            cfsettings.CefCommandLineArgs.Add("proxy-server", "localhost:8888");
            cfsettings.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36";
            cfsettings.IgnoreCertificateErrors = true;
            cfsettings.CachePath = System.IO.Path.Combine(path, "Cache");
            cfsettings.LogSeverity = LogSeverity.Info;
            cfsettings.PersistSessionCookies = true;
            cfsettings.LogFile = System.IO.Path.Combine(path, "log.txt");
            Cef.Initialize(cfsettings);

            InitializeComponent();
            //ChromiumWebBrowser.CacheMode=
            UrlTextBox.Text = Properties.Settings.Default.StartUrl;
            ChromiumWebBrowser.StatusMessage += ChromiumWebBrowser_StatusMessage;
            ChromiumWebBrowser.ConsoleMessage += ChromiumWebBrowser_ConsoleMessage;
            ChromiumWebBrowser.LoadingStateChanged += ChromiumWebBrowser_LoadingStateChanged;
            FiddlerApplication.AfterSessionComplete += FiddlerApplication_AfterSessionComplete;
            FiddlerApplication.Startup(8888, false, true, true);
        }

        private void ChromiumWebBrowser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
        }

        private void ChromiumWebBrowser_ConsoleMessage(object sender, ConsoleMessageEventArgs e)
        {
        }

        private void ChromiumWebBrowser_StatusMessage(object sender, StatusMessageEventArgs e)
        {
        }

        private void FiddlerApplication_AfterSessionComplete(Session osession)
        {
            if (osession.RequestMethod == "CONNECT" || osession.RequestMethod == "OPTIONS")
                return;
            string headers = osession.oRequest.headers.ToString();
            var reqBody = Encoding.UTF8.GetString(osession.RequestBody);
            string firstLine = osession.RequestMethod + " " + osession.fullUrl + " " + osession.oRequest.headers.HTTPVersion;
            int at = headers.IndexOf("\r\n");
            if (at < 0)
                return;
            headers = firstLine + Environment.NewLine + headers.Substring(at + 1);

            var res = Encoding.UTF8.GetString(osession.ResponseBody);
            string output = headers + Environment.NewLine +
                            (!string.IsNullOrEmpty(reqBody) ? reqBody + Environment.NewLine : string.Empty) +
                            Environment.NewLine;

            Console.WriteLine(output);
            Application.Current.Dispatcher.Invoke(() =>
            {
                var packet = new PacketInfomation
                {
                    RequestMethod = osession.RequestMethod,
                    ResponseCode = osession.responseCode,
                    IsSeleted = false,
                    Url = osession.fullUrl,
                    Details = output,
                    Response = res,
                    Body = reqBody
                };
                foreach (var item in osession.RequestHeaders)
                {
                    packet.Headers.Add(item.Name, item.Value);
                }
                _packetInfomations.Add(packet);
            });
        }

        public void Stop()
        {
            FiddlerApplication.AfterSessionComplete -= FiddlerApplication_AfterSessionComplete;
            if (FiddlerApplication.IsStarted())
                FiddlerApplication.Shutdown();
        }

        private void GotoButton_OnClick(object sender, RoutedEventArgs e)
        {
            ChromiumWebBrowser.Address = UrlTextBox.Text;
            Properties.Settings.Default.StartUrl = UrlTextBox.Text;
            Properties.Settings.Default.Save();
        }
    }
}