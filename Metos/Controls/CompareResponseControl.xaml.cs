﻿using Metos.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Metos.Controls
{
    /// <summary>
    /// Interaction logic for CompareResponseControl.xaml
    /// </summary>
    public partial class CompareResponseControl : IBaseControl
    {
        private readonly ObservableCollection<PacketInfomation> _packetInfomations;
        public readonly ObservableCollection<PacketInfomationResult> PacketInfomationResultList = new ObservableCollection<PacketInfomationResult>();
        public readonly ObservableCollection<UrlReplaceData> UrlReplaceDataList = new ObservableCollection<UrlReplaceData>();
        private Thread _thread;

        public CompareResponseControl(ObservableCollection<PacketInfomation> packetInfomations, PacketInfomation packetInfomation)
        {
            _packetInfomations = packetInfomations;
            InitializeComponent();
            ResultDataGrid.ItemsSource = PacketInfomationResultList;
            UrlDataGrid.ItemsSource = UrlReplaceDataList;
        }

        private void OnExcution(bool isexcuting)
        {
            ExcuteButton.IsEnabled = !isexcuting;
            AddButton.IsEnabled = !isexcuting;
            RemoveButton.IsEnabled = !isexcuting;
        }

        private void ExcuteButton_OnClick(object sender, RoutedEventArgs e)
        {
            List<PacketInfomationResult> selectedItemsList = PacketInfomationResultList.ToList();
            OnExcution(true);
            selectedItemsList.ForEach(q =>
            {
                q.NewResponse = "";
                q.Result = "--";
                q.Status = "Pending";
            });
            foreach (var packetInfomation in selectedItemsList)
            {
                var newUrl = packetInfomation.Url;
                foreach (var urlReplaceData in UrlReplaceDataList.ToList())
                {
                    newUrl = newUrl.Replace(urlReplaceData.Url, urlReplaceData.ReplaceWithUrl);
                }
                packetInfomation.NewUrl = newUrl;
            }
            if (_thread == null || !_thread.IsAlive)
            {
                _thread = new Thread(() =>
                {
                    foreach (var packetInfomation in selectedItemsList)
                    {
                        Excute(packetInfomation, UrlReplaceDataList.ToList());
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        OnExcution(false);
                    });
                });
            }
            _thread.Start();
        }

        private void Excute(PacketInfomationResult packetInfomation, List<UrlReplaceData> urlReplaceDataList)
        {
            packetInfomation.Status = "Started";
            var url = packetInfomation.Url;
            var newUrl = packetInfomation.NewUrl;

            HttpResponseMessage call1Result;
            try
            {
                using (var client1 = new HttpClient())
                {
                    foreach (var packetInfomationHeader in packetInfomation.Headers)
                    {
                        client1.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key, packetInfomationHeader.Value);
                    }
                    if (packetInfomation.RequestMethod == "GET")
                    {
                        call1Result = client1.GetAsync(new Uri(url)).Result;
                    }
                    else
                    {
                        var content = new StringContent(packetInfomation.Body);
                        call1Result = client1.PostAsync(new Uri(url), content).Result;
                    }
                }

                using (var client2 = new HttpClient())
                {
                    HttpResponseMessage call2Result;
                    foreach (var packetInfomationHeader in packetInfomation.Headers)
                    {
                        client2.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key, packetInfomationHeader.Value);
                    }
                    if (packetInfomation.RequestMethod == "GET")
                    {
                        call2Result = client2.GetAsync(new Uri(newUrl)).Result;
                    }
                    else
                    {
                        var content = new StringContent(packetInfomation.Body);
                        call2Result = client2.PostAsync(new Uri(newUrl), content).Result;
                    }
                    var result = call1Result.Content.ReadAsStringAsync().Result;
                    var result2 = call2Result.Content.ReadAsStringAsync().Result;
                    packetInfomation.Response = result;
                    packetInfomation.NewResponse = result2;
                    packetInfomation.Result = result == result2 ? "Match" : "Diff";
                }
                packetInfomation.Status = "Completed";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                packetInfomation.Status = "Failed";
                packetInfomation.Result = "--";
            }
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var packetInfomations = _packetInfomations.ToList();

            foreach (var packetInfomation in packetInfomations.Where(q => q.IsSeleted))
            {
                if (PacketInfomationResultList.All(q => q.Url != packetInfomation.Url))
                {
                    PacketInfomationResultList.Add(new PacketInfomationResult(packetInfomation));
                }
            }
        }

        private void RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            List<PacketInfomationResult> selectedItemsList = ResultDataGrid.SelectedItems.Cast<PacketInfomationResult>().ToList();
            foreach (PacketInfomationResult packetInfomation in selectedItemsList)
            {
                PacketInfomationResultList.Remove(packetInfomation);
            }
        }

        private void ResultDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var result = (PacketInfomationResult)ResultDataGrid.SelectedItem;
            if (result != null)
            {
                UrlTextBox.Text = result.Response;
                ResultTextBox.Text = result.NewResponse;
            }
        }

        private void AddUrlButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ReplaceUrlTextBox.Text) && !string.IsNullOrEmpty(ReplaceToUrlTextBox.Text))
            {
                UrlReplaceDataList.Add(new UrlReplaceData
                {
                    Url = ReplaceUrlTextBox.Text,
                    ReplaceWithUrl = ReplaceToUrlTextBox.Text
                });
            }
        }

        private void RemoveUrlButton_OnClick(object sender, RoutedEventArgs e)
        {
            List<UrlReplaceData> selectedItemsList = UrlDataGrid.SelectedItems.Cast<UrlReplaceData>().ToList();
            foreach (UrlReplaceData urlReplaceData in selectedItemsList)
            {
                UrlReplaceDataList.Remove(urlReplaceData);
            }
        }

        private void UrlDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            _thread.Abort();
            OnExcution(false);
        }
    }
}