﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Metos.Infrastructure.Models;
using Microsoft.CSharp;
using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Path = System.IO.Path;

namespace Metos.Controls
{
    /// <summary>
    ///     Interaction logic for CompareResponseControl.xaml
    /// </summary>
    public partial class CustomControl : IBaseControl
    {
        private readonly ObservableCollection<PacketInfomation> _packetInfomations;
        private readonly PacketInfomation _packetInfomation;

        public readonly ObservableCollection<PacketInfomationResult> PacketInfomationResultList =
            new ObservableCollection<PacketInfomationResult>();

        public ObservableCollection<string> Templetes { get; set; }

        public readonly ObservableCollection<UrlReplaceData>
            ResultDataList = new ObservableCollection<UrlReplaceData>();

        private Thread _thread;
        private ViewerManager _viewerManager;

        public CustomControl(ObservableCollection<PacketInfomation> packetInfomations,
            PacketInfomation packetInfomation)
        {
            InitializeComponent();
            Templetes = new ObservableCollection<string>();
            //TempletesDropDownButton.ItemsSource = Templetes;
            _packetInfomations = packetInfomations;
            _packetInfomation = packetInfomation;
            ResultDataGrid.ItemsSource = PacketInfomationResultList;
            var resourceName = "!new.cs";
            LoadTest(resourceName);

            foreach (var enumerateFile in Directory.EnumerateFiles("Template/", "*.cs"))
            {
                var name = Path.GetFileNameWithoutExtension(enumerateFile);
                if (!name.Contains("!"))
                {
                    var menuItem = new MenuItem() { Header = name.Replace('_', ' ') };
                    menuItem.Click += MenuItem_Click;
                    TempletesDropDownButton.Items.Add(menuItem);
                }
            }
        }

        private async void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var parentWindow = (MetroWindow)Window.GetWindow(this);

            var result = await parentWindow.ShowMessageAsync("Load Templete",
                "This will overwrite the existing code, continue?", MessageDialogStyle.AffirmativeAndNegative);
            if (result == MessageDialogResult.Affirmative)
            {
                var item = (MenuItem)sender;
                LoadTest($"{item.Header.ToString().Replace(' ', '_')}.cs");
            }
        }

        private void ExcuteButton_OnClick(object sender, RoutedEventArgs e)
        {
            TextEditor.Text = UpdateCode(_viewerManager, TextEditor.Text);
            var embededCodes = new List<string> { TextEditor.Text };
            var selectedItemsList = PacketInfomationResultList.ToList();
            UrlTextBox.Text = "";
            OnExcution(true);
            var result = "";
            selectedItemsList.ForEach(q =>
            {
                q.NewResponse = "";
                q.Result = "--";
                q.Status = "Pending";
                q.Size = 0;
                q.ResponceTime = new TimeSpan();
            });
            if (_thread == null || !_thread.IsAlive)
                _thread = new Thread(() =>
                {
                    try
                    {
                        result = Excute(selectedItemsList, embededCodes, _packetInfomation);
                    }
                    catch (Exception exception)
                    {
                        Application.Current.Invoke(() =>
                        {
                            var parentWindow = (MetroWindow)Window.GetWindow(this);
                            var message = "";
                            do
                            {
                                message = exception.Message;
                                exception = exception.InnerException;
                            } while (exception != null);

                            parentWindow.ShowMessageAsync("Error", message);
                        });
                    }
                    finally
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            UrlTextBox.Text = result;
                            OnExcution(false);
                        });
                    }
                });
            _thread.Start();
        }

        private static string GetFile(string resourceName, bool fromTemplete = true)
        {
            return fromTemplete ? File.ReadAllText($"Template/{resourceName}") : File.ReadAllText(resourceName);
        }

        private string Excute(List<PacketInfomationResult> packetInfomations, List<string> embededCodes,
            PacketInfomation packetInfomation)
        {
            var provider = new CSharpCodeProvider();
            var parameters = new CompilerParameters();

            parameters.ReferencedAssemblies.Add("System.Drawing.dll");
            parameters.ReferencedAssemblies.Add("System.dll");
            parameters.ReferencedAssemblies.Add("System.Net.Http.dll");
            parameters.ReferencedAssemblies.Add("Newtonsoft.Json.dll");
            parameters.ReferencedAssemblies.Add("Metos.Infrastructure.dll");
            parameters.GenerateInMemory = true;
            var results = provider.CompileAssemblyFromSource(parameters, embededCodes.ToArray());
            if (results.Errors.HasErrors)
            {
                var sb = new StringBuilder();

                foreach (CompilerError error in results.Errors)
                    sb.AppendLine($"Error ({error.ErrorNumber}): {error.ErrorText}");

                throw new InvalidOperationException(sb.ToString());
            }

            var assembly = results.CompiledAssembly;
            var program = assembly.GetType("Metos.CustomCode.Play");
            var main = program.GetMethod("Excute");
            var newPacket = new PacketInfomationResult();
            newPacket.Load(packetInfomation);
            return (string)main.Invoke(null, new object[] { packetInfomations, newPacket });
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var packetInfomations = _packetInfomations.ToList();

            foreach (var packetInfomation in packetInfomations.Where(q => q.IsSeleted))
            {
                var packetInfomationResult = new PacketInfomationResult(packetInfomation);
                packetInfomationResult.NewUrl = packetInfomationResult.Url;
                PacketInfomationResultList.Add(packetInfomationResult);
            }
        }

        private void RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedItemsList = ResultDataGrid.SelectedItems.Cast<PacketInfomationResult>().ToList();
            foreach (var packetInfomation in selectedItemsList) PacketInfomationResultList.Remove(packetInfomation);
        }

        private void ResultDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ResultDataGrid.SelectedItem != null)
            {
                var result = new PacketInfomation();
                result.Load((PacketInfomation)ResultDataGrid.SelectedItem);
            }
        }

        private void OnExcution(bool isexcuting)
        {
            ExcuteButton.IsEnabled = !isexcuting;
            AddButton.IsEnabled = !isexcuting;
            RemoveButton.IsEnabled = !isexcuting;
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            _thread?.Abort();
            OnExcution(false);
        }

        private async void EditorNewButton_OnClick(object sender, RoutedEventArgs e)
        {
            var parentWindow = (MetroWindow)Window.GetWindow(this);

            var result = await parentWindow.ShowMessageAsync("New File",
                "This will overwrite the existing code, continue?", MessageDialogStyle.AffirmativeAndNegative);
            if (result == MessageDialogResult.Affirmative)
            {
                var resourceName = "!new.cs";
                LoadTest(resourceName);
            }
        }

        private void EditorSaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = "csharp files(*.cs)|*.cs";
            var res = fd.ShowDialog();
            if (res.HasValue && res.Value)
            {
                File.WriteAllText(fd.FileName, TextEditor.Text);
            }
        }

        private void EditorLoadButton_OnClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog { Filter = "csharp files(*.cs)|*.cs" };
            var res = fd.ShowDialog();
            if (res.HasValue && res.Value)
            {
                LoadTest(fd.FileName, false);
            }
        }

        private void ShowCodeButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (TextEditor.Visibility == Visibility.Collapsed)
            {
                TextEditor.Visibility = Visibility.Visible;
                InputScrollViewer.Visibility = Visibility.Collapsed;
                TextEditor.Text = UpdateCode(_viewerManager, TextEditor.Text);
            }
            else
            {
                TextEditor.Visibility = Visibility.Collapsed;
                InputScrollViewer.Visibility = Visibility.Visible;
                LoadViewer(TextEditor.Text);
            }
        }

        private string UpdateCode(ViewerManager viewerManager, string code)
        {
            string newCode = code;
            Regex regex = new Regex(@"const[ \t]*([^\s]+)[ \t]*([^\s]+)[ \t]*=[ \t]*(([^\s]+));");
            var matches = regex.Matches(code);
            foreach (var data in viewerManager.ConstantDatas)
            {
                foreach (Match match in matches)
                {
                    if (match.Groups[2].Value == data.Name)
                    {
                        newCode = newCode.Replace(match.Groups[0].Value,
                            $"const {match.Groups[1].Value} {match.Groups[2].Value} = {data.Value};");
                        break;
                    }
                }
            }

            return newCode;
        }

        private void LoadTest(string resourceName, bool fromTemplete = true)
        {
            TextEditor.Text = GetFile(resourceName, fromTemplete);
            if (TextEditor.Visibility == Visibility.Collapsed)
            {
                LoadViewer(TextEditor.Text);
            }
        }

        private void LoadViewer(string code)
        {
            List<ConstantData> constantDatas = new List<ConstantData>();
            _viewerManager = new ViewerManager(InputScrollViewer, constantDatas);
            Regex regex = new Regex(@"const[ \t]*([^\s]+)[ \t]*([^\s]+)[ \t]*=[ \t]*(([^\s]+));");
            var matches = regex.Matches(code);
            foreach (Match match in matches)
            {
                var g1 = match.Groups[0];
                var g2 = match.Groups[1];
                var g3 = match.Groups[2];
                var g4 = match.Groups[3];
                constantDatas.Add(new ConstantData(g2.Value, g3.Value, g4.Value));
            }

            _viewerManager.Render();
        }
    }

    public class ViewerManager
    {
        private readonly ScrollViewer _scrollViewer;
        public readonly List<ConstantData> ConstantDatas;

        public ViewerManager(ScrollViewer scrollViewer, List<ConstantData> types)
        {
            _scrollViewer = scrollViewer;
            ConstantDatas = types;
        }

        public void Render()
        {
            var panel = new StackPanel { Orientation = Orientation.Vertical };
            panel.Children.Add(new Label { Width = 150, Content = "", HorizontalContentAlignment = HorizontalAlignment.Right });
            foreach (var item in ConstantDatas)
            {
                var component = GetComponent(item);
                if (component != null)
                    panel.Children.Add(component);
            }

            _scrollViewer.Content = panel;
        }

        private StackPanel GetComponent(ConstantData item)
        {
            try
            {
                var panel = new StackPanel
                {
                    Margin = new Thickness(2),
                    Orientation = Orientation.Horizontal
                };

                panel.Children.Add(new Label { Width = 150, Content = item.DisplayName, HorizontalContentAlignment = HorizontalAlignment.Right });
                Control control = null;
                switch (item.Type)
                {
                    case "int":
                    case "long":
                        NumericUpDown numericUpDown = new NumericUpDown() { Interval = 1, Value = long.Parse(item.Value) };
                        numericUpDown.ValueChanged += item.ValueChanged;
                        control = numericUpDown;
                        break;

                    case "double":
                    case "float":
                        numericUpDown = new NumericUpDown() { Interval = .1, Value = double.Parse(item.Value) };
                        numericUpDown.ValueChanged += item.ValueChanged;
                        control = numericUpDown;
                        break;

                    case "string":
                        TextBox textBox = new TextBox() { Text = item.Value };
                        textBox.KeyUp += item.KeyUp;
                        control = textBox;
                        break;
                    case "bool":
                        CheckBox checkBox = new CheckBox() { IsChecked = bool.Parse(item.Value) };
                        checkBox.Unchecked += item.Checked;
                        checkBox.Checked += item.Checked;
                        control = checkBox;
                        break;
                }

                if (control != null)
                {
                    control.Width = 200;
                    panel.Children.Add(control);
                    return panel;
                }
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

    }

    public class ConstantData
    {
        private string _name;
        public string Type { get; set; }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                DisplayName = Regex.Replace(_name, "([a-z])_?([A-Z])", "$1 $2");
            }
        }

        public string DisplayName { get; private set; }
        public string Value { get; set; }

        public ConstantData()
        {
        }

        public ConstantData(string type, string name, string value)
        {
            Type = type;
            Name = name;
            Value = value;
        }

        public void ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            if (e.NewValue != null) Value = e.NewValue.Value.ToString(CultureInfo.InvariantCulture);
        }

        public void KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Value = ((TextBox)sender).Text;
        }

        public void Checked(object sender, RoutedEventArgs e)
        {
            Value = ((CheckBox)sender).IsChecked.Value?"true":"false";
        }
    }
}