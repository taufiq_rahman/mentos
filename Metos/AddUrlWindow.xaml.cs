﻿using Metos.Infrastructure.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Metos
{
    /// <summary>
    /// Interaction logic for AddUrl.xaml
    /// </summary>
    public partial class AddUrlWindow
    {
        public PacketInfomation PacketInfomation { get; set; }

        public AddUrlWindow(PacketInfomation selectedItem)
        {
            InitializeComponent();
            PacketInfomation = selectedItem;
            UrlTextBox.Text = PacketInfomation.Url;
            HeaderTextBox.Text = JsonConvert.SerializeObject(PacketInfomation.Headers);
            MethordCombo.Text = PacketInfomation.RequestMethod;
            BodyTextBox.Text = PacketInfomation.Body;
        }

        public AddUrlWindow()
        {
            InitializeComponent();
            PacketInfomation = new PacketInfomation();
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.PacketInfomation = null;
            this.Close();
        }

        private void OkButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var url = UrlTextBox.Text;

                var dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(HeaderTextBox.Text);
                PacketInfomation.Url = url;
                PacketInfomation.Headers = dic;
                PacketInfomation.RequestMethod = MethordCombo.Text;
                PacketInfomation.Body = BodyTextBox.Text;
                this.Close();
            }
            catch (Exception)
            {
            }
        }
    }
}