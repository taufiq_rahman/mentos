﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Metos.Infrastructure.Models;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
namespace Metos.CustomCode
{
    public class Play
    {
        /// <summary>
        /// Call Two Server
        /// </summary>
        private const int Timeout = 10;
        private const int NumberofThreads = 1;
        private const string UrlBase = "www.current.server.com";
        private const string ReplaceWithUrl = "www.test.server.com";
        public static string Excute(List<PacketInfomationResult> packetInfomations, PacketInfomationResult selectedpacketInfomation)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Parallel.For(0, packetInfomations.Count,
                new ParallelOptions { MaxDegreeOfParallelism = Convert.ToInt32(NumberofThreads) }, (s) =>
                {
                    Run(packetInfomations[s]);
                    Thread.Sleep(Timeout);
                });

            var result = string.Format("S1={0},S2={1} ", UrlBase, ReplaceWithUrl);
            for (int i = 0; i < packetInfomations.Count; i++)
            {
                result += Environment.NewLine + packetInfomations[i].Result;
            }

            result += Environment.NewLine + string.Format("Completed in {0}ms", stopwatch.Elapsed.TotalSeconds);

            return result;
        }

        public static void Run(PacketInfomationResult packetInfomation)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var t1EndTime = new TimeSpan();
            var t2EndTime = new TimeSpan();
            packetInfomation.Status = "Started";
            var url = packetInfomation.Url;
            var newUrl = url.Replace(UrlBase, ReplaceWithUrl);
            Console.WriteLine(newUrl);

            try
            {
                var t1 = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        using (var httpClient = new HttpClient())
                        {
                            HttpResponseMessage httpResponseMessage;
                            foreach (var packetInfomationHeader in packetInfomation.Headers)
                            {
                                httpClient.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key,
                                    packetInfomationHeader.Value);
                            }

                            if (packetInfomation.RequestMethod == "GET")
                            {
                                httpResponseMessage = httpClient.GetAsync(new Uri(url)).Result;
                            }
                            else
                            {
                                var content = new StringContent(packetInfomation.Body);
                                httpResponseMessage = httpClient.PostAsync(new Uri(url), content).Result;
                            }

                            var result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                            var bodylength = httpResponseMessage.Content.Headers.ContentLength.Value;
                            var headerlength = httpResponseMessage.Headers.ToString().Length;
                            packetInfomation.Size = bodylength + headerlength;
                        }
                        packetInfomation.ResponceTime = stopwatch.Elapsed;
                        t1EndTime = stopwatch.Elapsed;
                    }
                    catch (Exception exception)
                    {
                    }
                });
                var t2 = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        using (var httpClient = new HttpClient())
                        {
                            HttpResponseMessage httpResponseMessage;
                            foreach (var packetInfomationHeader in packetInfomation.Headers)
                            {
                                httpClient.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key,
                                    packetInfomationHeader.Value);
                            }

                            if (packetInfomation.RequestMethod == "GET")
                            {
                                httpResponseMessage = httpClient.GetAsync(new Uri(newUrl)).Result;
                            }
                            else
                            {
                                var content = new StringContent(packetInfomation.Body);
                                httpResponseMessage = httpClient.PostAsync(new Uri(newUrl), content).Result;
                            }

                            var result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                            var bodylength = httpResponseMessage.Content.Headers.ContentLength.Value;
                            var headerlength = httpResponseMessage.Headers.ToString().Length;
                            packetInfomation.Size = bodylength + headerlength;
                        }
                        packetInfomation.ResponceTime = stopwatch.Elapsed;
                        t2EndTime = stopwatch.Elapsed;
                    }
                    catch (Exception exception)
                    {
                    }
                });
                var e = new Task[] { t1, t2 };
                Task.WaitAll(e);
                //packetInfomation.Status = string.Format("Completed {0}s ", stopwatch.Elapsed.TotalMilliseconds);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (t1EndTime.TotalSeconds == 0 && t2EndTime.TotalSeconds > 0)
                {
                    UpdateStatus("Failed", t2EndTime.TotalMilliseconds.ToString("F"), packetInfomation);
                }
                else if (t1EndTime.TotalSeconds > 0 && t2EndTime.TotalSeconds == 0)
                {
                    UpdateStatus(t1EndTime.TotalMilliseconds.ToString("F"), "Failed", packetInfomation);
                }
                else if (t1EndTime.TotalSeconds > 0 && t2EndTime.TotalSeconds > 0)
                {
                    UpdateStatus(t1EndTime.TotalMilliseconds.ToString("F"), t2EndTime.TotalMilliseconds.ToString("F"), packetInfomation);
                }
                else
                {
                    UpdateStatus("Failed", "Failed", packetInfomation);
                }
            }
        }

        public static void UpdateStatus(string t1EndTime, string t2EndTime, PacketInfomationResult packetInfomation)
        {
            packetInfomation.Status = string.Format("S1:{0}|S2:{1}[{2}]", t1EndTime, t2EndTime, packetInfomation.Size);
            var url = packetInfomation.Url.Replace(UrlBase, "");
            packetInfomation.Result = string.Format("S1: {0} |S2: {1} [{3}]| Url: {2}", t1EndTime, t2EndTime, url, packetInfomation.Size);
        }
    }
}