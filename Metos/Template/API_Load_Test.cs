﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Metos.Infrastructure.Models;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
namespace Metos.CustomCode
{
    /// <summary>
    /// Api Load Test
    /// By Taufiq Abdur Rahman
    /// </summary>
    public class Play
    {
        private const int Timeout = 0;
        private const int NumberofThreads = 3;
        private const int NumberofThreadPerEndPoint = 3;
        private const int HitsPerEndpoint = 20;

        public static string Excute(List<PacketInfomationResult> packetInfomations, PacketInfomationResult selectedpacketInfomation)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Parallel.For(0, packetInfomations.Count,
                new ParallelOptions { MaxDegreeOfParallelism = Convert.ToInt32(NumberofThreads) }, (s) =>
                {
                    Stopwatch stopwatch2 = new Stopwatch();
                    stopwatch2.Start();
                    var packetInfomation = packetInfomations[s];
                    packetInfomation.Status = "Started";
                    int x = 0;
                    Parallel.For(0, HitsPerEndpoint,
                        new ParallelOptions { MaxDegreeOfParallelism = Convert.ToInt32(NumberofThreadPerEndPoint) }, (r) =>
                        {
                            Run(packetInfomation);

                            Thread.Sleep(Timeout);
                            packetInfomation.Status = string.Format("[{2}/{3}][{1} byte]", packetInfomation.ResponceTime.TotalMilliseconds.ToString("F"), packetInfomation.Size, x++, HitsPerEndpoint);
                        });
                    packetInfomation.ResponceTime += stopwatch2.Elapsed;
                    packetInfomation.Status = string.Format("[{0} ms][{1} byte]", packetInfomation.ResponceTime.TotalMilliseconds.ToString("F"), packetInfomation.Size);
                    packetInfomation.Result = string.Format("[{0} ms][{1} byte]{2}", packetInfomation.ResponceTime.TotalMilliseconds.ToString("F"), packetInfomation.Size, packetInfomation.Url);
                });
            var result = string.Format("{0} Calls per api", HitsPerEndpoint);
            for (int i = 0; i < packetInfomations.Count; i++)
            {
                result += Environment.NewLine + packetInfomations[i].Result;
            }
            return result + Environment.NewLine + string.Format("Completed in {0}s", stopwatch.Elapsed.TotalSeconds);
        }

        public static void Run(PacketInfomationResult packetInfomation)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var url = packetInfomation.Url;
            long bodylength = 0;
            long headerlength = 0;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    HttpResponseMessage httpResponseMessage;
                    foreach (var packetInfomationHeader in packetInfomation.Headers)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key, packetInfomationHeader.Value);
                    }
                    if (packetInfomation.RequestMethod == "GET")
                    {
                        httpResponseMessage = httpClient.GetAsync(new Uri(url)).Result;
                    }
                    else
                    {
                        var content = new StringContent(packetInfomation.Body);
                        httpResponseMessage = httpClient.PostAsync(new Uri(url), content).Result;
                    }
                    bodylength = httpResponseMessage.Content.Headers.ContentLength.Value;
                    headerlength = httpResponseMessage.Headers.ToString().Length;

                    var result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                    packetInfomation.Size += bodylength + headerlength;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                packetInfomation.Status = "Failed";
            }
        }
    }
}