﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Metos.Infrastructure.Models;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
namespace Metos.CustomCode
{
    /// <summary>
    /// End Point Performance Test
    /// By Taufiq Abdur Rahman
    /// </summary>
    public class Play
    {
        private const int Timeout = 10;
        private const int NumberofThreads = 3;
        public static string Excute(List<PacketInfomationResult> packetInfomations, PacketInfomationResult selectedpacketInfomation)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Parallel.For(0, packetInfomations.Count,
                new ParallelOptions { MaxDegreeOfParallelism = Convert.ToInt32(NumberofThreads) }, (s) =>
                {
                    Run(packetInfomations[s]);
                    Thread.Sleep(Timeout);
                });
            var result = "";
            for (int i = 0; i < packetInfomations.Count; i++)
            {
                result += Environment.NewLine + packetInfomations[i].Result;
            }
            return result + Environment.NewLine + string.Format("Completed in {0}s", stopwatch.Elapsed.TotalSeconds);
        }

        public static void Run(PacketInfomationResult packetInfomation)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            packetInfomation.Status = "Started";
            var url = packetInfomation.Url;
            long bodylength = 0;
            long headerlength = 0;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    HttpResponseMessage httpResponseMessage;
                    foreach (var packetInfomationHeader in packetInfomation.Headers)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(packetInfomationHeader.Key, packetInfomationHeader.Value);
                    }
                    if (packetInfomation.RequestMethod == "GET")
                    {
                        httpResponseMessage = httpClient.GetAsync(new Uri(url)).Result;
                    }
                    else
                    {
                        var content = new StringContent(packetInfomation.Body);
                        httpResponseMessage = httpClient.PostAsync(new Uri(url), content).Result;
                    }

                    var result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                    bodylength = httpResponseMessage.Content.Headers.ContentLength.Value;
                    headerlength = httpResponseMessage.Headers.ToString().Length;
                    packetInfomation.Size += bodylength + headerlength;
                }

                packetInfomation.ResponceTime = stopwatch.Elapsed;
                packetInfomation.Status = string.Format("[{0} ms][{1} byte]", stopwatch.Elapsed.TotalMilliseconds.ToString("F"), packetInfomation.Size);
                packetInfomation.Result = string.Format("[{0} ms][{1} byte]{2}", stopwatch.Elapsed.TotalMilliseconds.ToString("F"), packetInfomation.Size, packetInfomation.Url);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                packetInfomation.Status = "Failed";
            }
        }
    }
}