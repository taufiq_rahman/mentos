﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Metos.Infrastructure.Models
{
    /// <summary>
    ///     Devoloped By Taufiq Abdur Rahman
    ///     Interaction logic for CaptureControl.xaml
    /// </summary>
    public class PacketInfomation : INotifyPropertyChanged
    {
        private string _url;

        public PacketInfomation()
        {
            Headers = new Dictionary<string, string>();
        }

        public bool IsSeleted { get; set; }

        public string Url
        {
            get => _url;
            set
            {
                _url = value;
                OnPropertyChanged(nameof(Url));
            }
        }

        public string Details { get; set; }

        [JsonIgnore] public string Response { get; set; }
        [JsonIgnore] public long Size { get; set; }

        public Dictionary<string, string> Headers { get; set; }
        public string RequestMethod { get; set; }
        public string Body { get; set; }
        public int ResponseCode { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler SelectionChanged;

        public void Load(PacketInfomation packetInfomation)
        {
            Headers = new Dictionary<string, string>();
            RequestMethod = packetInfomation.RequestMethod;
            Url = packetInfomation.Url;
            Details = packetInfomation.Details;
            IsSeleted = packetInfomation.IsSeleted;
            Headers = packetInfomation.Headers;
            Response = packetInfomation.Response;
            Body = packetInfomation.Body;
            OnSelectionChanged();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnSelectionChanged()
        {
            SelectionChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}