﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Metos.Infrastructure.Models
{
    public class PacketInfomationResult : PacketInfomation
    {
        private string _newUrl;
        private TimeSpan _responceTime;
        private string _result;
        private string _status;

        public PacketInfomationResult()
        {
            Headers = new Dictionary<string, string>();
        }

        public PacketInfomationResult(PacketInfomation packetInfomation)
        {
            Headers = new Dictionary<string, string>();
            RequestMethod = packetInfomation.RequestMethod;
            Url = packetInfomation.Url;
            Details = packetInfomation.Details;
            IsSeleted = packetInfomation.IsSeleted;
            Headers = packetInfomation.Headers;
            Response = packetInfomation.Response;
            Body = packetInfomation.Body;
            Status = "Pending";
            Result = "--";
        }

        [JsonIgnore] public string NewResponse { get; set; }

        public string Status
        {
            get => _status;
            set
            {
                _status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        public TimeSpan ResponceTime
        {
            get => _responceTime;
            set
            {
                _responceTime = value;
                OnPropertyChanged(nameof(ResponceTime));
            }
        }

        public string Result
        {
            get => _result;
            set
            {
                _result = value;
                OnPropertyChanged(nameof(Result));
            }
        }

        public string NewUrl
        {
            get => _newUrl;
            set
            {
                _newUrl = value;
                OnPropertyChanged(nameof(NewUrl));
            }
        }
    }
}