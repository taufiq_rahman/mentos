﻿namespace Metos.Infrastructure.Models
{
    public class UrlReplaceData
    {
        public string Url { get; set; }
        public string ReplaceWithUrl { get; set; }
    }
}